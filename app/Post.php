<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{

	protected $guarded = [];

	// protected $fillable = [
	//     'name', 'email', 'password', 'username',
	// ];

    public function user()
    {
   		return $this->belongsTo(User::class);
    }

    public function favorites()
    {
    	return $this->belongsToMany(User::class);
    }

    public function isFavorited()
    {
        return (auth()->user()) ? auth()->user()->favorites->contains($this->id) : false;
    }
}
