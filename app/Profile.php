<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
	protected $guarded = [];
	
    public function user()
    {
    	return $this->belongsTo(User::class);
    }

    public function profileImage()
    {
    	return ($this->image) ? '/storage/' . $this->image : 'https://www.clipartwiki.com/clipimg/detail/197-1979569_no-profile.png'; 
    }

    public function followers()
    {
        return $this->belongsToMany(User::class);
    }
}
